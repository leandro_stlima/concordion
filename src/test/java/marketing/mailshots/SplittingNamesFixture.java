package marketing.mailshots;

import Utils.NameSplitter;
import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.runner.RunWith;

@RunWith(ConcordionRunner.class)
public class SplittingNamesFixture {

    public NameSplitter.Name split(String fullName) {
        return NameSplitter.split(fullName);
    }
}
