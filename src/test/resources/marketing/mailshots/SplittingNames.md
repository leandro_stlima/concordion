# Splitting Names

To help personalise our mailshots we want to have the first name and last name of the customer. 
Unfortunately the customer data that we are supplied only contains full names.

The system therefore attempts to break a supplied full name into its constituents by splitting 
around whitespace.

### [Example](- "basic")
The full name [Jane Smith](- "#name") is [broken](- "#result = split(#name)") into first name [Jane](- "?=#result.firstName") and last name [Smith](- "?=#result.lastName").

### [Example](- "singleName")
The full name [Sting](- "#name") is [broken](- "#result = split(#name)") into first name [Sting](- "?=#result.firstName") and no last name.

### [Example](- "fullNameWithTitle")
The full name [Sir Bob Geldof](- "#name") is [broken](- "#result = split(#name)") into first name [Bob](- "?=#result.firstName") and last name [Geldof](- "?=#result.lastName").

### [Example](- "fullNameWithOtherNames")
The full name [Maria de los Santos](- "#name") is [broken](- "#result = split(#name)") into first name [Maria](- "?=#result.firstName") and last name [de los Santos](- "?=#result.lastName").