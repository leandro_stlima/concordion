package Utils;

import com.google.common.collect.ImmutableSet;

public class NameSplitter {

    public static final ImmutableSet<String> HONORIFFICS = ImmutableSet.of(
            "Master",
            "Mr",
            "Miss",
            "Ms",
            "Mrs",
            "Mx",
            "Sir",
            "Madam",
            "Ma'am",
            "Dame",
            "Lord",
            "Lady",
            "Adv"
    );

    public static Name split(String fullName) {
        final Name name = new Name();
        final String[] st = fullName.split(" ");
        final int tokens = st.length;
        int index = 0;
        if (index < tokens) {
            final String first = st[index++];
            if (HONORIFFICS.contains(first)) {
                name.honoriffic = first;
                if (index < tokens)
                    name.firstName = st[index++];
            } else
                name.firstName = first;

            int reverseIndex = tokens - 1;
            if (index <= reverseIndex) {
                String lastName = st[reverseIndex--];
                String word;
                while (index <= reverseIndex && (word = st[reverseIndex]).equals(word.toLowerCase()))
                    lastName = st[reverseIndex--].concat(" ").concat(lastName);
                name.lastName = lastName;
            }

            if (index < tokens) {
                String surname = st[index++];
                while (index < tokens)
                    surname = surname.concat(" ").concat(st[index++]);
                name.surname = surname;
            }
        }
        return name;
    }

    public static class Name {

        public String honoriffic = "";
        public String firstName = "";
        public String lastName = "";
        public String surname = "";
    }
}
